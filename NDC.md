# Note de Clarification

Est-on susceptible d'avoir de nouvelles plateformes, ou doit-on uniquement gérer les 4 ?
=> Oui, de nouvelles plateformes peuvent émerger et doivent être gérables

Les jeux et les applications font-elles partie d'un groupe dont les OS ne font pas partie ? (exemple des exécutables)
=> À nous de le gérer

Plusieurs personnes peuvent-elles avoir le même nom prénom ?
=> Oui, la clé doit prendre en compte (Nom, Prénom, DateDeNaissance)

Si oui y-a-t'il un critère qui permet de les différencier?
=> Cf plus haut : (Nom, Prénom, DateDeNaissance) clé

Peut-on acheter un OS ? Ou n'achetons nous que des exécutables ?
=> Oui on peut aussi acheter un OS

On a décidé de prendre nom comme klé de la relation logiciel puisqu'on s'est dit que des deux c'était probablement la plus immuable.
