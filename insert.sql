
INSERT INTO Employe
VALUES ('Rezlan', 'Jean', to_date('1982-11-24', 'YYYY-MM-DD'),1 ),
('Vilar', 'Pedro', to_date('1981-10-01', 'YYYY-MM-DD'),2),
('Durand', 'Guillaume', to_date('1995-02-11', 'YYYY-MM-DD'),3 ),
('Terni', 'John', to_date('1997-01-15', 'YYYY-MM-DD'),4);

INSERT INTO Client
VALUES
('Revah', 'Sylvie', to_date('1988-04-15', 'YYYY-MM-DD'),'5 rue de la madelaine','0389898900'),
('Revah', 'David', to_date('1974-12-18', 'YYYY-MM-DD'),'5 rue de la madelaine','0389898900'),
('Rodriguez', 'Caroline', to_date('1990-04-17', 'YYYY-MM-DD'),'9 avenue du capitaine','0606060608'),
('Koch', 'Sylvie', to_date('1986-06-10', 'YYYY-MM-DD'),'2 résidence avé césar','0606060609');

INSERT INTO Logiciel
VALUES
('Paint','Project Photo',to_date('1996-07-18', 'YYYY-MM-DD')),
('PhotoMega','ExplosionFutur',to_date('1789-01-01', 'YYYY-MM-DD')),
('League of Legend','Project MOBA',to_date('2010-05-17', 'YYYY-MM-DD')),
('Windows 10','Projet fenetre',to_date('2017-01-12', 'YYYY-MM-DD')),
('Ubuntu','Projet penguin',to_date('2012-07-04', 'YYYY-MM-DD'));


INSERT INTO Executable
VALUES
('Paint',NULL,'Editeur d''image','Application'),
('PhotoMega',NULL,'Editeur extreme de photo en feu','Application'),
('League of Legend','MOBA',NULL,'Jeu');

INSERT INTO OS
VALUES
('Windows 10'),
('Ubuntu');

INSERT INTO Plateforme
VALUES
('PC'),
('Mobile'),
('Console'),
('Tablette');

INSERT INTO Compatible
VALUES
('Paint','Windows 10'),
('PhotoMega','Windows 10'),
('League of Legend','Windows 10'),
('Paint','Ubuntu'),
('League of Legend','Ubuntu');

INSERT INTO Supporte
VALUES('PC','Ubuntu'),
('Tablette','Windows 10'),
('PC','Windows 10'),
('Mobile','Windows 10');


INSERT INTO Version
VALUES
('Paint',10,2,to_date('2020-02-04', 'YYYY-MM-DD')),
('PhotoMega',3,0,to_date('1800-04-04', 'YYYY-MM-DD')),
('League of Legend',2,0,to_date('2020-02-04', 'YYYY-MM-DD')),
('League of Legend',1,1,to_date('2019-11-28', 'YYYY-MM-DD')),
('Paint',8,1,to_date('2019-08-04', 'YYYY-MM-DD')),
('Windows 10',2,0,to_date('2019-07-07', 'YYYY-MM-DD')),
('Ubuntu',2,0,to_date('2019-09-07', 'YYYY-MM-DD'));

INSERT INTO Achat
VALUES
('Paint','Revah','Sylvie',to_date('1988-04-15', 'YYYY-MM-DD'),to_date('2020-02-07', 'YYYY-MM-DD')),
('Windows 10','Revah','Sylvie',to_date('1988-04-15', 'YYYY-MM-DD'),to_date('2020-02-09', 'YYYY-MM-DD')),
('Windows 10','Revah','Sylvie',to_date('1988-04-15', 'YYYY-MM-DD'),to_date('2020-01-09', 'YYYY-MM-DD')),
('League of Legend','Koch', 'Sylvie', to_date('1986-06-10', 'YYYY-MM-DD'),to_date('2020-01-01', 'YYYY-MM-DD')),
('Ubuntu','Revah', 'David', to_date('1974-12-18', 'YYYY-MM-DD'),to_date('2019-10-10', 'YYYY-MM-DD'));

INSERT INTO Probleme
VALUES
('League of Legend',2 ,0, 'Rodriguez','Caroline',to_date('1990-04-17', 'YYYY-MM-DD'), to_date('2020-02-04', 'YYYY-MM-DD'), 'Je suis sûre je l''ai acheté'),
('League of Legend',2 ,0, 'Koch','Sylvie',to_date('1986-06-10', 'YYYY-MM-DD'), to_date('2020-02-04', 'YYYY-MM-DD'), 'Teemo est OP'),
('Windows 10',2 ,0, 'Revah','Sylvie',to_date('1988-04-15', 'YYYY-MM-DD'), to_date('2020-03-14', 'YYYY-MM-DD'), 'Le clic gauche detruit la couche d''ozone'),
('Paint',8 ,1, 'Revah','David',to_date('1974-12-18', 'YYYY-MM-DD'), to_date('2020-01-02', 'YYYY-MM-DD'), 'Le rouge est trop rouge'),
('PhotoMega',3,0, 'Revah','David',to_date('1974-12-18', 'YYYY-MM-DD'), to_date('1800-04-04', 'YYYY-MM-DD'), 'Ce logiciel trop extreme a entraine la combustion instantanee de mon chien.');

INSERT INTO Correctif
VALUES
('League of Legend',2,0,'Koch','Sylvie',to_date('1986-06-10', 'YYYY-MM-DD'), to_date('2020-02-04', 'YYYY-MM-DD'),to_date('2020-02-05', 'YYYY-MM-DD'),'Patch',7,'Terni', 'John', to_date('1997-01-15', 'YYYY-MM-DD'), NULL),
('Windows 10',2 ,0, 'Revah','Sylvie',to_date('1988-04-15', 'YYYY-MM-DD'), to_date('2020-03-14', 'YYYY-MM-DD'),to_date('2020-03-22', 'YYYY-MM-DD'),'Instructions',NULL,'Terni', 'John', to_date('1997-01-15', 'YYYY-MM-DD'), 'Il ne faut pas brancher son PC sur un reacteur atomique.'),
('PhotoMega',3 ,0, 'Revah','David',to_date('1974-12-18', 'YYYY-MM-DD'), to_date('1800-04-04', 'YYYY-MM-DD'),to_date('1904-07-23', 'YYYY-MM-DD'),'Instructions',NULL,'Terni', 'John', to_date('1997-01-15', 'YYYY-MM-DD'), 'Toutes mes condoleances pour votre chien madame le president'),
('PhotoMega',3 ,0, 'Revah','David',to_date('1974-12-18', 'YYYY-MM-DD'), to_date('1800-04-04', 'YYYY-MM-DD'),to_date('1907-06-24', 'YYYY-MM-DD'),'Patch',1,'Terni', 'John', to_date('1997-01-15', 'YYYY-MM-DD'), NULL);
