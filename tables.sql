CREATE TABLE Client(
  nom VARCHAR(40),
  prenom VARCHAR(40),
  date_naissance DATE,
  adresse VARCHAR(100) NOT NULL,
  num_tel VARCHAR(10) NOT NULL,
  PRIMARY KEY(nom, prenom, date_naissance)
  );

CREATE TABLE Employe(
  nom VARCHAR(40),
  prenom VARCHAR(40),
  date_naissance DATE,
  numero_de_poste INTEGER NOT NULL,
  PRIMARY KEY(nom, prenom, date_naissance)
);


CREATE TABLE Plateforme(
  nom VARCHAR(40) PRIMARY KEY
);


CREATE TABLE Logiciel(
  nom VARCHAR(100) PRIMARY KEY,
  nom_dev VARCHAR(100) UNIQUE NOT NULL,
  date_de_lancement DATE NOT NULL
  );

CREATE TYPE Texe AS ENUM ('Application', 'Jeu');

CREATE TABLE Executable(
  nom VARCHAR(40) PRIMARY KEY REFERENCES Logiciel(nom),
  type_jeu VARCHAR(40),
  type_application VARCHAR(40),
  type Texe NOT NULL,
  CHECK ((type = 'Application' AND type_jeu IS NULL AND type_application IS NOT NULL) OR (type = 'Jeu' AND type_application IS NULL AND type_jeu IS NOT NULL ))-- attention = comme c'est une énum
);



CREATE TABLE Os(
  nom VARCHAR PRIMARY KEY REFERENCES Logiciel(nom)
);

CREATE TABLE Compatible(
  Executable VARCHAR REFERENCES Logiciel (nom),
  OS VARCHAR REFERENCES Logiciel (nom)
);

CREATE TABLE Supporte(
  plateforme VARCHAR REFERENCES Plateforme (nom),
  os VARCHAR REFERENCES Os(nom),
  PRIMARY KEY(plateforme, os)
);

CREATE TABLE Version(
  nom_log VARCHAR REFERENCES Logiciel(nom),
  semVer_Major INTEGER,
  semVer_Minor INTEGER,
  date_disp DATE,
  PRIMARY KEY(nom_log, semVer_Major, semVer_Minor)
);

CREATE TABLE Probleme(
  nom_log VARCHAR,
  semVer_Major INTEGER ,
  semVer_Minor INTEGER,--partie Version
  nom VARCHAR,
  prenom VARCHAR,
  date_naissance DATE,-- partie Client
  date_pb TIMESTAMP,
  description TEXT NOT NULL,
  FOREIGN KEY (nom_log, semVer_Major, semVer_Minor) REFERENCES Version(nom_log, semVer_Major, semVer_Minor),
  FOREIGN KEY (nom, prenom, date_naissance)   REFERENCES Client(nom, prenom, date_naissance),
  PRIMARY KEY (nom_log, semVer_Major, semVer_Minor, nom, prenom, date_naissance, date_pb) --ça c'est de la clé
);

CREATE TYPE Tcor AS ENUM ('Patch', 'Instructions');

CREATE TABLE Correctif(
  nom_log VARCHAR,
  semVer_Major INTEGER ,
  semVer_Minor INTEGER,
  nom VARCHAR,
  prenom VARCHAR,
  date_naissance DATE,
  date_pb TIMESTAMP,--problème
  date_cor TIMESTAMP,
  type Tcor NOT NULL,
  numero_de_patch INTEGER,
  r_nom VARCHAR NOT NULL,
  r_prenom VARCHAR NOT NULL,
  r_date_naissance DATE NOT NULL,
  instruction_a_suivre TEXT,
  CHECK ((type = 'Patch' and numero_de_patch IS NOT NULL AND instruction_a_suivre IS NULL ) OR (type = 'Instructions' and instruction_a_suivre IS NOT NULL)),
  FOREIGN KEY (nom_log, semVer_Major, semVer_Minor, nom, prenom, date_naissance, date_pb) REFERENCES Probleme(nom_log, semVer_Major, semVer_Minor, nom, prenom, date_naissance, date_pb),
  FOREIGN KEY (r_nom, r_prenom, r_date_naissance) REFERENCES Employe(nom, prenom, date_naissance),
  PRIMARY KEY (nom_log, semVer_Major, semVer_Minor, nom, prenom, date_naissance, date_pb, date_cor) --ça c'est de la clé
);

CREATE TABLE Achat(
  nom_log VARCHAR REFERENCES Logiciel(nom),
  nom VARCHAR,
  prenom VARCHAR,
  date_naissance DATE,
  date_achat TIMESTAMP,
  FOREIGN KEY (nom, prenom, date_naissance) REFERENCES Client(nom, prenom, date_naissance),
  PRIMARY KEY (nom, prenom, date_naissance, nom_log, date_achat)
);
