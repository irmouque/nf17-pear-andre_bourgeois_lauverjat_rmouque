DEBUT DU RELATIONNEL

---------------------- PARTIE GAUCHE DE L'UML


Executable(#id_exe=>Logiciel, type:{jeu,application}, type_jeu:string, type_application:string)         //héritage par mère

  CONTRAINTES :
    - type NOT NULL
    - (type_jeu NOT NULL and type=jeu) OR (type_application NOT NULL and type=application)
    - NOT (type_jeu NOT NULL and type=application)
    - NOT (type_application NOT NULL and type=jeu)
  VUES :
    - vJeu=restriction(Executable, Executable.type=jeu)
    - vAppli=restriction(Executable, Executable.type=application)
  METHODES :
    - AppTabletteCorriger()

OS(#id_OS=>Logiciel)

Logiciel(#nom:string, nom_dev:string, date_de_lancement:date)                //héritage par référence

  CONTRAINTES :
    - nom_dev UNIQUE
    - (nom_dev,date_de_lancement) NOT NULL
    - Intersection (PROJECTION(Executable, id_exe), PROJECTION(OS, id_OS)) = {}
    - Projection (Logiciel, nom) = Union(PROJECTION(Executable, id_exe), PROJECTION(OS, id_OS))
  VUES :
    - vExe = JointureNaturelle(Logiciel, Executable)
    - vOs = JointureNaturelle(Logiciel, OS)
  METHODES :
    - NbProblemes()

Plateforme(#nom:string)
Compatible(#id_OS=>OS, #id_exe=>Executable)                  //classe association entre exécutable et OS
Supporte(#id_OS=>OS, #plateforme=>Plateforme)                //classe association entre OS et plateforme




---------------------- PARTIE DROITE DE L'UML


Version(#version:Semver,#nom=>Logiciel,SemVer_Major:int,SemVer_Minor:int,date_disp:date)   // rappatriement du datatype dans version

Client(#nom:string,#prenom:string,#date_naissance:date,adresse:string,num_tel:string)        //héritage par fille
Employe(#nom:string,#prenom:string,#date_naissance:date,numero_de_poste:timestamp)

Achat(#date_achat:timestamp, #nom=>Client.nom,#prenom=>Client.prenom,#date_naissance=>Client.date_naissance, #nom_l=>Logiciel)


Probleme(#date_pb:timestamp,#nom=>Client.nom,#prenom=>Client.prenom,#date_naissance=>Client.date_naissance,#nom_logiciel=>Logiciel,#version_maj=>Version.SemVer_Major,#version_min=>Version.SemVer_Minor,desc:text)       //classe association client-version

Correctif(#date_cor:timestamp,#date_pb=>Probleme,#nom_c=>Client.nom,#prenom_c=>Client.prenom,#date_naissance_c=>Client.date_naissance,#nom_l=>Logiciel,#version_maj=>Version.SemVer_Major,#version_min=>Version.SemVer_Minor,#nom_e=>Employe.nom,#prenom_e=>Employe.prenom,#date_naissance_e=>Employe.date_naissance,instruction_a_suivre:text,numero_de_patch:int,type={Instructions,Patch})  //héritage par mère

  CONTRAINTES :
    - type NOT NULL
    - (instruction_a_suivre NOT NULL and type=Instructions) OR (numero_de_patch NOT NULL and type=Patch)
    - NOT (instruction_a_suivre NOT NULL and type=Patch)
    - NOT (numero_de_patch NOT NULL and type=Instructions)
  VUES :
    - vInstruction=Restriction (Correctif, type=1)
    - vPatch=Restriction (Correctif, type=2)

    CONTRAINTES :
     - date_cor > date_pb

###################

Requête whatClientPbOnNotOwned en algègbre relationnele:
     - R1=JointureExterneGauche(Probleme, Achat, Achat.nom=Probleme.nom and Probleme.nom_log=Achat.nom_log)
     - R2=Restriction(R1, date_achat is NULL)
     - R4=Projection(R2,nom,prenom,date_naissance)
