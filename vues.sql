
CREATE VIEW vOs AS
SELECT o.nom,l.nom_dev, l.date_de_lancement  from Os o
Join Logiciel l
ON l.nom = o.nom;

CREATE VIEW vJeu AS
SELECT l.nom, l.nom_dev, l.date_de_lancement, e.type_jeu
from Executable e
Join Logiciel l
ON l.nom = e.nom
Where e.type = 'Jeu';

CREATE VIEW vAppli AS
SELECT l.nom, l.nom_dev, l.date_de_lancement, e.type_application
from Executable e
Join Logiciel l
ON l.nom = e.nom
Where e.type = 'Application';

CREATE VIEW vPatch AS
SELECT nom_log, semVer_Major, semVer_Minor, nom,prenom, date_naissance, date_pb, date_cor, numero_de_patch, r_nom, r_prenom, r_date_naissance
FROM Correctif
Where type='Patch';

CREATE VIEW vInstructions AS
SELECT nom_log, semVer_Major, semVer_Minor, nom,prenom, date_naissance, date_pb, date_cor, instruction_a_suivre, r_nom, r_prenom, r_date_naissance
FROM Correctif
Where type='Instructions';


--NOMBRE PROBLEMES NON CORRIGES POUR CHAQUE VERSION DE SES LOGICIELS

CREATE VIEW vNbProblemes AS
SELECT p.nom_log, p.semVer_Major, p.semVer_Minor, COUNT(p.nom_log) AS nbPb
FROM Probleme p LEFT OUTER JOIN Correctif c ON p.nom_log=c.nom_log
AND p.semVer_Major=c.semVer_Major
AND p.semVer_Minor=c.semVer_Minor
AND p.nom=c.nom
AND p.prenom=c.prenom
AND p.date_naissance=c.date_naissance
AND p.date_pb=c.date_pb
WHERE c.nom_log IS NULL
GROUP BY (p.nom_log, p.semVer_Major, p.semVer_Minor)
ORDER BY nbPb;

--APPLICATIONS TOURNANT SUR TABLETTE ET + D'UN Correctif

CREATE VIEW AppTabletteCorriger AS
SELECT l.nom, l.nom_dev, l.date_de_lancement, e.type_application
FROM Logiciel l, Executable e, Compatible c, Supporte s, Correctif co
WHERE l.nom=e.nom
AND e.type='Application'
AND e.nom=c.Executable
AND c.OS=s.os
AND s.plateforme='Tablette'
AND co.nom_log = l.nom
GROUP BY l.nom, l.nom_dev, l.date_de_lancement, e.type_application
HAVING COUNT(*)>1;

--CLIENTS ONT REMONTE UN PBE SANS AVOIR LE LOGICIEL


CREATE VIEW vWhatClientPbOnNotOwned AS
SELECT p.nom, p.prenom,p.date_naissance
FROM Probleme p
LEFT JOIN achat a
ON a.nom = p.nom AND a.nom_log = p.nom_log
WHERE a.date_achat IS NULL
;
